import ChatDashboard from './ChatDashboard';
import UserProfile from './UserProfile';

export {ChatDashboard, UserProfile};
