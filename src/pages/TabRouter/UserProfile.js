import React, {useState, useEffect} from 'react';
import {ScrollView, View, Text, StatusBar, TextInput} from 'react-native';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import {Avatar} from '@rneui/themed';
import {Button} from '@rneui/base';
import {useMutation} from 'react-query';
import {showMessage} from 'react-native-flash-message';
import ImagePicker from 'react-native-image-crop-picker';
import auth from '@react-native-firebase/auth';
import storage from '@react-native-firebase/storage';
import {firebase} from '@react-native-firebase/database';
import Icon from 'react-native-vector-icons/Feather';
import moment from 'moment';

import {getInitials} from '../../helpers';
import styles, {EStyleSheet} from '../../styles/pages/Chat/UserProfile';

const UserProfile = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const database = firebase
    .app()
    .database(
      'https://chatapp-c84e8-default-rtdb.asia-southeast1.firebasedatabase.app/',
    );
  let currentUser = auth().currentUser;
  const [userProfile, setUserProfile] = useState();
  const [currentProfile, setCurrentProfile] = useState();

  const getUserProfile = async () => {
    await database
      .ref(`/users/${currentUser.uid}`)
      .once('value')
      .then(snapshot => {
        setUserProfile(snapshot.val());
      });
  };

  const getRealTimeUserProfile = () => {
    database.ref(`/users/${currentUser.uid}`).on('value', snapshot => {
      setCurrentProfile(snapshot.val());
    });
  };

  const getAvatarURL = async reference => {
    return await storage().ref(reference).getDownloadURL();
  };

  useEffect(() => {
    getUserProfile();
    getRealTimeUserProfile();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const {mutate: logOutMutation} = useMutation(
    async () => {
      const response = await auth().signOut();
      return response;
    },
    {
      throwOnError: true,
      onSuccess: () => {
        showMessage({
          message: 'Log out success.',
          type: 'danger',
          backgroundColor: EStyleSheet.value('$success[500]'),
          titleStyle: {textAlign: 'center'},
          floating: true,
          statusBarHeight: insets.top + 6,
        });
        navigation.replace('Login');
      },
      onError: error => {
        showMessage({
          message: error.message,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: {textAlign: 'center'},
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    },
  );

  const {mutate: uploadAvatar, isLoading: isUpload} = useMutation(
    async upload => {
      setUserProfile({
        ...userProfile,
        photoURL: upload.path,
      });
      const response = await storage()
        .ref(
          `images/profiles/${currentUser.uid}_${moment().format(
            'DDMMYYYYHHmmss',
          )}${upload.type}`,
        )
        .putFile(decodeURI(upload.path));
      return response;
    },
    {
      throwOnError: true,
      onSuccess: async data => {
        await database
          .ref(`/users/${currentUser.uid}`)
          .update({
            photoURL: await getAvatarURL(data.metadata.fullPath),
          })
          .catch(error => {
            setUserProfile({
              ...userProfile,
              photoURL: currentProfile?.photoURL,
            });
            throw error;
          });
      },
      onError: error => {
        setUserProfile({
          ...userProfile,
          photoURL: currentProfile?.photoURL,
        });
        showMessage({
          message: error.message,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: {textAlign: 'center'},
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    },
  );

  return (
    <SafeAreaView style={styles.safeArea} edges={['top', 'left', 'right']}>
      <StatusBar barStyle="light-content" />
      <View style={styles.profile}>
        <Avatar
          rounded
          size={160}
          containerStyle={styles.avatarContainer}
          source={userProfile?.photoURL ? {uri: userProfile?.photoURL} : {}}
          title={getInitials(userProfile?.displayName)}>
          <Avatar.Accessory
            size={32}
            style={styles.avatarAccessory}
            onPress={() => {
              !isUpload &&
                ImagePicker.openPicker({
                  width: 480,
                  height: 480,
                  mediaType: 'photo',
                  cropping: true,
                  cropperCircleOverlay: true,
                  forceJpg: true,
                  compressImageMaxWidth: 360,
                  compressImageMaxHeight: 360,
                  compressImageQuality: 0.5,
                }).then(async image => {
                  const fileType = image.path.match(/\.[0-9a-z]+$/i)[0];
                  uploadAvatar({
                    path: image.path,
                    type: fileType,
                  });
                });
            }}
          />
        </Avatar>
        <TextInput
          style={styles.displayName}
          value={userProfile?.displayName}
          placeholder="Type a Name..."
          textAlign="center"
          placeholderTextColor={EStyleSheet.value('$secondary[500]')}
          onChange={event =>
            setUserProfile({
              ...userProfile,
              displayName: event.nativeEvent.text,
            })
          }
          onBlur={async event => {
            const value = event.nativeEvent.text.replace(/\s+/g, ' ').trim();
            if (value.length !== 0) {
              if (value !== currentProfile?.displayName || !currentProfile) {
                setUserProfile({
                  ...userProfile,
                  displayName: value,
                });
                await database.ref(`/users/${currentUser.uid}`).update({
                  displayName: value,
                });
              }
            } else {
              setUserProfile({
                ...userProfile,
                displayName: currentProfile?.displayName,
              });
              showMessage({
                message: 'Please fill valid name!',
                type: 'danger',
                backgroundColor: EStyleSheet.value('$danger[500]'),
                titleStyle: {textAlign: 'center'},
                floating: true,
                statusBarHeight: insets.top + 6,
              });
            }
          }}
        />
      </View>
      <SafeAreaView style={styles.safeAreaContent} edges={['left', 'right']}>
        <ScrollView contentContainerStyle={styles.content}>
          <View style={styles.bioContainer}>
            <Text style={styles.bioHeader}>About</Text>
            <TextInput
              style={styles.bioBox}
              placeholder="type a bio..."
              defaultValue={userProfile?.bio}
              placeholderTextColor={EStyleSheet.value('$base.secondary')}
              multiline={true}
              onBlur={event =>
                database.ref(`/users/${currentUser.uid}`).update({
                  bio: event.nativeEvent.text,
                })
              }
            />
          </View>
          <Button
            buttonStyle={styles.logoutButton}
            titleStyle={styles.logoutTittle}
            disabledStyle={{opacity: 0.5}}
            icon={<Icon name="power" style={styles.logoutIcon} />}
            loadingProps={{animating: true}}
            onPress={logOutMutation}
            title="Logout"
          />
        </ScrollView>
      </SafeAreaView>
    </SafeAreaView>
  );
};

export default UserProfile;
