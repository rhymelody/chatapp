import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {ChatList, ChatUsers} from '../ScreenRouter/Chat';

const ChatDashboard = () => {
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Navigator
      initialRouteName="Chat List"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Chat List" component={ChatList} />
      <Stack.Screen name="All User" component={ChatUsers} />
    </Stack.Navigator>
  );
};

export default ChatDashboard;
