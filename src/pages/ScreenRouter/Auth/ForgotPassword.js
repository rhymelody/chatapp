import React from 'react';
import {
  StatusBar,
  ScrollView,
  View,
  KeyboardAvoidingView,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';
import {useMutation} from 'react-query';
import {useForm} from 'react-hook-form';
import {showMessage} from 'react-native-flash-message';
import {yupResolver} from '@hookform/resolvers/yup';
import auth from '@react-native-firebase/auth';

import {forgotPasswordValidation} from '../../../configs/form-validation';
import {Input} from '../../../components/Extras';
import styles from '../../../styles/pages/Auth/ForgotPassword';

function ForgotPassword({navigation}) {
  const insets = useSafeAreaInsets();

  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    defaultValues: {
      email: '',
      password: '',
    },
    resolver: yupResolver(forgotPasswordValidation),
  });

  const {mutate: forgotPasswordMutation, isLoading} = useMutation(
    async forgotPasswordData => {
      const response = await auth().sendPasswordResetEmail(
        forgotPasswordData.email,
      );
      return response;
    },
    {
      throwOnError: true,
      onSuccess: () => {
        showMessage({
          message: 'Reset password link successfully sent, check your email!',
          type: 'danger',
          backgroundColor: EStyleSheet.value('$success[500]'),
          titleStyle: {textAlign: 'center'},
          floating: true,
          statusBarHeight: insets.top + 6,
        });
        navigation.replace('Login');
      },
      onError: error => {
        showMessage({
          message: error.message,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: {textAlign: 'center'},
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    },
  );

  const onSubmit = async data => {
    forgotPasswordMutation(data);
  };

  return (
    <>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="light-content"
      />
      <SafeAreaView style={styles.header} edges={['right', 'top', 'left']}>
        <TouchableOpacity
          testID="back_to_login_button"
          onPress={() => navigation.goBack()}>
          <View style={styles.headerActionButton}>
            <Icon name="arrow-back-outline" style={styles.headerActionIcon} />
          </View>
        </TouchableOpacity>
      </SafeAreaView>
      <SafeAreaView style={styles.safeArea} edges={['right', 'bottom', 'left']}>
        <ScrollView contentContainerStyle={styles.container}>
          <KeyboardAvoidingView behavior="position" style={styles.form}>
            <Text style={styles.formHeader}>Forgot Password</Text>
            <Text style={styles.formDescription}>
              Reset password link will be sent via Email, make sure your Email
              active.
            </Text>
            <Input
              name="email"
              placeholder="Email"
              icon="at-sign"
              testID="email_input"
              keyboardType="email-address"
              control={control}
              errors={errors}
              disable={isLoading}
            />
            <TouchableOpacity
              testID="forgotPassword_button"
              style={[
                styles.formButton,
                !isLoading
                  ? styles.formButtonActive
                  : styles.formButtonDisabled,
              ]}
              onPress={handleSubmit(onSubmit)}
              disabled={isLoading}>
              {isLoading && (
                <ActivityIndicator
                  size="small"
                  style={styles.formButton.indicator}
                  color={styles.formButton.indicator.color}
                />
              )}
              <Text style={styles.formButtonText}>Submit</Text>
            </TouchableOpacity>
            <View style={styles.navigationContainer}>
              <Text style={styles.navigationText}>
                Already remember your password?
              </Text>
              <TouchableOpacity
                testID="login_link"
                onPress={() => navigation.navigate('Login')}>
                <Text style={styles.navigationLink}>Login</Text>
              </TouchableOpacity>
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

ForgotPassword.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
    navigate: PropTypes.func.isRequired,
    replace: PropTypes.func.isRequired,
  }).isRequired,
};

export default ForgotPassword;
