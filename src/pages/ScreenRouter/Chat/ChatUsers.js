import React, {useState, useEffect} from 'react';
import {View, FlatList, StatusBar} from 'react-native';
import {ListItem, Avatar, SearchBar} from '@rneui/themed';
import {SafeAreaView} from 'react-native-safe-area-context';
import {firebase} from '@react-native-firebase/database';
import uuid from 'react-native-uuid';
import Icon from 'react-native-vector-icons/Feather';
import auth from '@react-native-firebase/auth';

import {getInitials} from '../../../helpers';
import styles from '../../../styles/pages/Chat/ChatUsers';

const ChatUsers = ({navigation}) => {
  const database = firebase
    .app()
    .database(
      'https://chatapp-c84e8-default-rtdb.asia-southeast1.firebasedatabase.app/',
    );

  let currentUser = auth().currentUser;
  const [currentProfile, setCurrentProfile] = useState();
  const [search, setSearch] = useState('');
  const [allUser, setAllUser] = useState([]);
  const [allUserBackup, setAllUserBackup] = useState([]);

  const getRealTimeUserProfile = () => {
    database.ref(`/users/${currentUser.uid}`).on('value', snapshot => {
      setCurrentProfile(snapshot.val());
    });
  };

  const getAllUser = () => {
    database
      .ref('users/')
      .once('value')
      .then(snapshot => {
        let filteredUsers = Object.assign({}, snapshot.val());
        delete filteredUsers[currentUser.uid];
        filteredUsers = Object.values(filteredUsers).map((item, index) => {
          return {...item, uid: Object.keys(filteredUsers)[index]};
        });
        setAllUser(filteredUsers);
        setAllUserBackup(filteredUsers);
      });
  };

  useEffect(() => {
    getRealTimeUserProfile();
    getAllUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const searchUser = value => {
    setSearch(value);
    setAllUser(
      allUserBackup.filter(valSearch => valSearch.displayName.match(value)),
    );
  };

  const createChatList = data => {
    if (currentProfile) {
      database
        .ref('/chatlist/' + currentUser.uid + '/' + data.uid)
        .once('value')
        .then(snapshot => {
          if (snapshot.val() == null) {
            let roomID = uuid.v4();
            let currentUserData = {
              roomID,
              uid: currentUser.uid,
              displayName: currentProfile.displayName,
              photoURL: currentProfile.photoURL,
              about: currentProfile.bio,
              lastMessage: `Say Hi to ${currentProfile.displayName}!`,
            };
            database
              .ref('/chatlist/' + data.uid + '/' + currentUser.uid)
              .update(currentUserData);
            data.lastMessage = `Say Hi to ${data.displayName}!`;
            data.roomID = roomID;
            database
              .ref('/chatlist/' + currentUser.uid + '/' + data.uid)
              .update(data);
            navigation.navigate('Chat Room', {data: data});
          } else {
            navigation.navigate('Chat Room', {data: snapshot.val()});
          }
        });
    }
  };

  const renderItem = ({item}) => (
    <ListItem
      onPress={() => createChatList(item)}
      bottomDivider
      containerStyle={styles.userItem}>
      <Avatar
        source={{uri: item.photoURL}}
        rounded
        title={getInitials(item.displayName)}
        size="medium"
      />
      <ListItem.Content>
        <ListItem.Title style={styles.userName}>
          {item.displayName}
        </ListItem.Title>
        <ListItem.Subtitle style={styles.userBio} numberOfLines={1}>
          {item.bio}
        </ListItem.Subtitle>
      </ListItem.Content>
    </ListItem>
  );

  return (
    <SafeAreaView style={styles.safeArea} edges={['top', 'left', 'right']}>
      <StatusBar barStyle="light-content" />
      <View style={styles.headerContainer}>
        <Icon
          style={styles.icon}
          name="chevron-left"
          onPress={() => navigation.goBack()}
        />
        <SearchBar
          platform="ios"
          placeholder="Search by name..."
          onChangeText={value => searchUser(value)}
          value={search}
          containerStyle={styles.searchContainer}
          inputContainerStyle={styles.inputContainer}
          inputStyle={styles.searchInput}
          lightTheme
        />
      </View>
      <View style={styles.content}>
        <FlatList
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
          data={allUser}
          renderItem={renderItem}
        />
      </View>
    </SafeAreaView>
  );
};

export default ChatUsers;
