import React, {useState, useEffect} from 'react';
import {FlatList, StatusBar, TouchableOpacity} from 'react-native';
import {ListItem, Avatar} from '@rneui/themed';
import {SafeAreaView} from 'react-native-safe-area-context';
import {firebase} from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import Icon from 'react-native-vector-icons/Feather';

import {getInitials} from '../../../helpers';
import styles from '../../../styles/pages/Chat/ChatList';

const ChatList = ({navigation}) => {
  const database = firebase
    .app()
    .database(
      'https://chatapp-c84e8-default-rtdb.asia-southeast1.firebasedatabase.app/',
    );

  let currentUser = auth().currentUser;
  const [chatList, setChatList] = useState([]);

  const getChatList = async () => {
    database.ref('/chatlist/' + currentUser?.uid).on('value', snapshot => {
      setChatList(Object.values(snapshot.val() || {}));
    });
  };

  useEffect(() => {
    getChatList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderItem = ({item}) => (
    <ListItem
      bottomDivider
      activeOpacity={1}
      containerStyle={styles.chatList}
      onPress={() => navigation.navigate('Chat Room', {data: item})}>
      <Avatar
        source={{uri: item.photoURL}}
        rounded
        title={getInitials(item.displayName)}
        size="medium"
      />
      <ListItem.Content>
        <ListItem.Title style={styles.userName}>
          {item.displayName}
        </ListItem.Title>
        <ListItem.Subtitle style={styles.lastMessage} numberOfLines={1}>
          {item.lastMessage}
        </ListItem.Subtitle>
      </ListItem.Content>
    </ListItem>
  );

  return (
    <SafeAreaView style={styles.container} edges={['top', 'left', 'right']}>
      <StatusBar barStyle="dark-content" translucent={false} />
      {/* <HomeHeader /> */}
      <FlatList
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
        data={chatList}
        renderItem={renderItem}
      />
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('All User')}>
        <Icon name="users" style={styles.buttonIcon} />
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default ChatList;
