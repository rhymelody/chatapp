import React, {useEffect, useState} from 'react';
import {
  KeyboardAvoidingView,
  ImageBackground,
  TextInput,
  Platform,
  TouchableOpacity,
  FlatList,
  StatusBar,
} from 'react-native';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import EStyleSheet from 'react-native-extended-stylesheet';
import {showMessage} from 'react-native-flash-message';
import Icon from 'react-native-vector-icons/Feather';
import {firebase} from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import moment from 'moment';

import styles from '../../../styles/pages/Chat/ChatRoom';
import {ChatRoomHeader, ChatBubble} from '../../../components/Chat';

const ChatRoom = props => {
  const database = firebase
    .app()
    .database(
      'https://chatapp-c84e8-default-rtdb.asia-southeast1.firebasedatabase.app/',
    );

  let currentUser = auth().currentUser;
  const insets = useSafeAreaInsets();
  const {data} = props.route.params;
  const navigation = props.navigation;

  const [message, setMessage] = useState('');
  const [disabled, setDisabled] = useState(false);
  const [chat, setChat] = useState([]);

  useEffect(() => {
    const onChildAdd = database
      .ref('/messages/' + data.roomID)
      .on('child_added', snapshot => {
        setChat(state => [snapshot.val(), ...state]);
      });
    return () =>
      database.ref('/messages' + data.roomID).off('child_added', onChildAdd);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data.roomID]);

  const messageValid = text => text && text.replace(/\s/g, '').length;

  const sendMessage = () => {
    if (message === '' || messageValid(message) === 0) {
      showMessage({
        message: 'Enter something...',
        type: 'danger',
        backgroundColor: EStyleSheet.value('$danger[500]'),
        titleStyle: {textAlign: 'center'},
        floating: true,
        statusBarHeight: insets.top + 6,
      });
      return false;
    }

    setDisabled(true);

    let messageData = {
      roomID: data.roomID,
      message: message,
      from: currentUser?.uid,
      to: data.uid,
      sendTime: moment().format(''),
      messageType: 'text',
    };

    const newReference = database.ref('/messages/' + data.roomID).push();
    messageData.id = newReference.key;
    newReference.set(messageData).then(() => {
      let chatListUpdate = {
        lastMessage: message,
        sendTime: messageData.sendTime,
      };
      database
        .ref('/chatlist/' + data?.uid + '/' + currentUser?.uid)
        .update(chatListUpdate);
      database
        .ref('/chatlist/' + currentUser?.uid + '/' + data?.uid)
        .update(chatListUpdate);

      setMessage('');
      setDisabled(false);
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="light-content" translucent={false} />
      <ChatRoomHeader data={data} navigation={navigation} />
      <ImageBackground
        source={require('../../../assets/images/background.jpg')}
        style={styles.background}>
        <FlatList
          style={styles.chat}
          data={chat}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index}
          inverted
          renderItem={({item}) => {
            return (
              <ChatBubble
                sender={item.from === currentUser?.uid}
                message={item.message}
                item={item}
              />
            );
          }}
        />
      </ImageBackground>
      <KeyboardAvoidingView
        style={styles.messageArea}
        keyboardVerticalOffset={8}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <TextInput
          style={styles.textBox}
          placeholder="type a message"
          placeholderTextColor={EStyleSheet.value('$base.secondary')}
          multiline={true}
          value={message}
          onChangeText={value => setMessage(value)}
        />
        <TouchableOpacity disabled={disabled} onPress={sendMessage}>
          <Icon style={styles.sendIcon} name="send" />
        </TouchableOpacity>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default ChatRoom;
