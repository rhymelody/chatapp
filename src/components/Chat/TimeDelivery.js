import React from 'react';
import {View, Text} from 'react-native';
import moment from 'moment';
import EStyleSheet from 'react-native-extended-stylesheet';
import Icon from 'react-native-vector-icons/Feather';

import styles from '../../styles/components/Chat/TimeDelivery';

const TimeDelivery = props => {
  const {sender, item} = props;
  return (
    <View
      style={[
        styles.container,
        {
          justifyContent: sender ? 'flex-end' : 'flex-start',
        },
      ]}>
      <Text
        style={[
          styles.time,
          {
            color: sender
              ? EStyleSheet.value('$base.white')
              : EStyleSheet.value('$secondary[500]'),
          },
        ]}>
        {moment(item.send_time).format('LLL')}
      </Text>
      <Icon
        name="check"
        style={[
          styles.check,
          {
            color: item.seen
              ? EStyleSheet.value('$base.black')
              : EStyleSheet.value('$base.white'),
          },
        ]}
      />
    </View>
  );
};

export default TimeDelivery;
