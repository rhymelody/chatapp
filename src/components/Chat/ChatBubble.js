import React from 'react';
import {View, Text, Pressable} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import styles from '../../styles/components/Chat/ChatBubble';
import TimeDelivery from './TimeDelivery';

const ChatBubble = props => {
  const {sender, item} = props;
  return (
    <Pressable>
      <View style={[styles.buubleShape, sender ? styles.right : styles.left]} />
      <View
        style={[
          styles.container,
          {
            alignSelf: sender ? 'flex-end' : 'flex-start',
            backgroundColor: sender
              ? EStyleSheet.value('$primary[500]')
              : EStyleSheet.value('$base.white'),
          },
        ]}>
        <Text
          style={[
            styles.chatText,
            {
              color: sender
                ? EStyleSheet.value('$base.white')
                : EStyleSheet.value('$base.black'),
            },
          ]}>
          {item.message}
        </Text>
        <TimeDelivery sender={sender} item={item} />
      </View>
    </Pressable>
  );
};

export default ChatBubble;
