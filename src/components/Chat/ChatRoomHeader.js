import React from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {Avatar} from '@rneui/themed';

import {getInitials} from '../../helpers';
import styles from '../../styles/components/Chat/ChatRoomHeader';

const ChatRoomHeader = ({data, navigation}) => {
  return (
    <View style={styles.container}>
      <Icon
        style={styles.icon}
        name="chevron-left"
        onPress={() => navigation.goBack()}
      />
      <Avatar
        title={getInitials(data.displayName)}
        source={{uri: data.photoURL}}
        rounded
        size="small"
      />
      <View style={styles.nameContainer}>
        <Text numberOfLines={1} style={styles.name}>
          {data.displayName}
        </Text>
      </View>
    </View>
  );
};

export default ChatRoomHeader;
