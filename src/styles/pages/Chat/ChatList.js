import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {flex: 1, backgroundColor: '$base.white'},
  chatList: {paddingVertical: '.5rem', marginVertical: 0},
  userName: {
    fontWeight: '600',
    fontSize: '.925rem',
    marginBottom: '.25rem',
    color: '$netral[900]',
  },
  lastMessage: {
    fontSize: '.725rem',
    color: '$secondary[700]',
  },
  button: {
    position: 'absolute',
    bottom: '1rem',
    right: '1rem',
    width: '3rem',
    height: '3rem',
    borderRadius: '3rem / 2',
    backgroundColor: '$base.primary',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 5,
  },
  buttonIcon: {color: '$base.white', fontSize: '1.25rem'},
});

export default styles;
