import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '$base.primary',
  },
  background: {flex: 1},
  chat: {flex: 1},
  messageArea: {
    backgroundColor: '$base.primary',
    elevation: 5,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: '.5rem',
    marginBottom: '.5rem',
    paddingHorizontal: '1.125rem',
    justifyContent: 'space-between',
  },
  textBox: {
    backgroundColor: '$base.white',
    width: '90%',
    borderRadius: '.75rem',
    borderWidth: 0.5,
    borderColor: '$primary[300]',
    paddingHorizontal: '1rem',
    paddingVertical: '.35rem',
    color: '$base.black',
  },
  sendIcon: {
    marginLeft: '.25rem',
    fontSize: '1.125rem',
    color: '$base.white',
  },
});

export default styles;
