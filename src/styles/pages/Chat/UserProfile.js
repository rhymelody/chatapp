import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  safeArea: {
    backgroundColor: '$base.primary',
    flex: 1,
  },
  profile: {
    backgroundColor: '$base.primary',
    padding: '1rem',
    alignItems: 'center',
  },
  avatarContainer: {
    backgroundColor: '$base.secondary',
    marginBottom: '1rem',
  },
  avatarAccessory: {position: 'absolute', top: '75%', left: '75%'},
  displayName: {
    color: '$base.white',
    fontSize: '1.125rem',
    fontWeight: '600',
    marginVertical: '.25rem',
    padding: '.5rem',
    borderRadius: '.75rem',
    borderWidth: 0.25,
    borderColor: '$secondary[500]',
    textAlign: 'center',
    width: '75%',
  },
  safeAreaContent: {
    flex: 1,
    backgroundColor: '$base.white',
  },
  content: {
    paddingVertical: '1.5rem',
    flex: 1,
    alignItems: 'center',
  },
  bioContainer: {
    width: '80%',
    marginBottom: '2rem',
    alignItems: 'center',
  },
  bioHeader: {
    fontSize: '1rem',
    fontWeight: '500',
    marginBottom: '1rem',
  },
  bioBox: {
    backgroundColor: '$base.white',
    width: '100%',
    height: '5rem',
    borderRadius: '.5rem',
    borderWidth: 0.5,
    borderColor: '$primary[300]',
    padding: '1rem',
    color: '$base.black',
  },
  logoutButton: {
    paddingVertical: '.625rem',
    backgroundColor: '$base.primary',
    borderRadius: '.5rem',
    width: '100%',
  },
  logoutTittle: {
    fontSize: '.875rem',
  },
  logoutIcon: {
    fontSize: '1.125rem',
    color: '$base.white',
    marginRight: '1rem',
  },
});

export {EStyleSheet};
export default styles;
