import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  safeArea: {flex: 1, backgroundColor: '$base.primary'},
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingHorizontal: '.5rem',
  },
  icon: {
    color: '$base.white',
    fontSize: '2rem',
  },
  searchContainer: {
    elevation: 2,
    backgroundColor: '$base.primary',
    width: '90%',
  },
  inputContainer: {
    backgroundColor: '$base.white',
    paddingHorizontal: '.5rem',
  },
  searchInput: {
    fontSize: '.925rem',
    color: '$base.secondary',
  },
  content: {flex: 1, backgroundColor: '$base.white'},
  userItem: {paddingVertical: '.5rem', marginVertical: '.125rem'},
  userName: {
    fontWeight: '600',
    fontSize: '.925rem',
    marginBottom: '.25rem',
    color: '$netral[900]',
  },
  userBio: {
    fontSize: '.725rem',
    color: '$secondary[700]',
  },
});

export default styles;
