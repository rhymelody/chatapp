import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  safeArea: {
    backgroundColor: '$background',
    flexGrow: 1,
  },
  container: {
    backgroundColor: '$background',
    padding: '1.5rem',
    width: '100%',
    flexGrow: 1,
    justifyContent: 'center',
  },
  formHeader: {
    marginTop: '.675rem',
    color: '$netral[900]',
    fontSize: '1.325rem',
    marginBottom: '.5rem',
    fontWeight: '700',
    textAlign: 'center',
  },
  formDescription: {
    color: '$secondary[500]',
    fontSize: '.85rem',
    marginBottom: '2.5rem',
    textAlign: 'center',
  },
  form: {
    header: {
      color: '$netral[900]',
      fontSize: '1.5rem',
      fontWeight: '900',
      marginBottom: '1.5rem',
      textAlign: 'center',
    },
  },
  forgotLink: {
    marginTop: '-.325rem',
    marginBottom: '.325rem',
    paddingHorizontal: '.125rem',
    textAlign: 'right',
    fontSize: '.75rem',
    fontWeight: '600',
    color: '$netral[500]',
  },
  formButtonGroup: {
    alignItems: 'center',
    marginTop: '1rem',
    marginBottom: '2.25rem',
  },
  rowButtonGroup: {
    flexDirection: 'row',
    marginBottom: '.5rem',
    width: '100%',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  formButton: {
    paddingVertical: '.75rem',
    paddingHorizontal: '1rem',
    borderRadius: '.5rem',
    width: '82.5%',
    height: '2.75rem',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '$primary[500]',
    indicator: {
      color: '$base.white',
      marginRight: '1rem',
    },
  },
  formButtonActive: {
    opacity: 1,
  },
  formButtonDisabled: {
    opacity: 0.2,
  },
  formButtonText: {
    color: '$base.white',
    fontSize: '1rem',
    fontWeight: '600',
  },
  biometricButton: {
    paddingVertical: '.75rem',
    paddingHorizontal: '1rem',
    borderRadius: '.5rem',
    width: '15%',
    height: '2.75rem',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '$primary[500]',
  },
  googleSigninButton: {
    paddingVertical: '.75rem',
    paddingHorizontal: '1rem',
    borderRadius: '.5rem',
    width: '100%',
    height: '2.75rem',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '$primary[500]',
    icon: {
      color: '$base.white',
      fontSize: '1.375rem',
      bottom: 0.475,
      marginRight: '.75rem',
    },
  },
  navigationContainer: {
    alignItems: 'center',
  },
  navigationText: {
    fontSize: '.875rem',
    fontWeight: '500',
    color: '$secondary[500]',
    marginBottom: '.25rem',
  },
  navigationLink: {
    fontSize: '.875rem',
    fontWeight: '800',
    color: '$netral[500]',
  },
});

export default styles;
