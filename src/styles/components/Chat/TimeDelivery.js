import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '.25rem',
  },
  time: {
    fontSize: '.5rem',
  },
  check: {
    fontSize: '.875rem',
    marginLeft: '.5rem',
  },
});

export default styles;
