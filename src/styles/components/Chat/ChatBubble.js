import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    alignSelf: 'flex-end',
    marginHorizontal: '1rem',
    minWidth: '8rem',
    maxWidth: '80%',
    paddingHorizontal: '1rem',
    marginVertical: '.25rem',
    paddingVertical: '.425rem',
    borderRadius: 8,
  },
  chatText: {
    fontSize: '.725rem',
  },
  timeText: {
    fontSize: 10,
  },
  //   dayview: {
  //     alignSelf: 'center',
  //     height: 30,
  //     width: 100,
  //     justifyContent: 'center',
  //     alignItems: 'center',
  //     // backgroundColor: COLORS.white,
  //     borderRadius: 30,
  //     marginTop: 10,
  //   },
  //   iconView: {
  //     width: 42,
  //     height: 42,
  //     borderRadius: 21,
  //     alignItems: 'center',
  //     justifyContent: 'center',
  //     // backgroundColor: COLORS.themecolor,
  //   },
  buubleShape: {
    position: 'absolute',
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 15,
    borderRightWidth: 5,
    borderBottomWidth: 20,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
  },
  left: {
    borderBottomColor: '$base.white',
    left: 2,
    bottom: 10,
    transform: [{rotate: '0deg'}],
  },
  right: {
    borderBottomColor: '$base.primary',
    right: 2,
    bottom: 5,
    transform: [{rotate: '103deg'}],
  },
});

export default styles;
