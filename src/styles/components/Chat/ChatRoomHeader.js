import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    height: '3.5rem',
    backgroundColor: '$base.primary',
    elevation: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    marginHorizontal: '1rem',
    color: '$base.white',
    fontSize: '2rem',
  },
  nameContainer: {flex: 1, marginLeft: 10},
  name: {
    color: '$base.white',
    fontSize: 16,
    textTransform: 'capitalize',
  },
});

export default styles;
