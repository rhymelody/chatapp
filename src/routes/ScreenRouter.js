import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import analytics from '@react-native-firebase/analytics';

import {Login, Register, ForgotPassword} from '../pages/ScreenRouter/Auth';
import {ChatList, ChatRoom, ChatUsers} from '../pages/ScreenRouter/Chat';

import {TabRouter} from './';

const ScreenRouter = () => {
  const Stack = createNativeStackNavigator();
  const routeNameRef = React.useRef();
  const navigationRef = React.useRef();

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        routeNameRef.current = navigationRef.current.getCurrentRoute().name;
      }}
      onStateChange={async () => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = navigationRef.current.getCurrentRoute().name;

        if (previousRouteName !== currentRouteName) {
          await analytics().logScreenView({
            screen_name: currentRouteName,
            screen_class: currentRouteName,
          });
        }
        routeNameRef.current = currentRouteName;
      }}>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Forgot Password" component={ForgotPassword} />
        <Stack.Screen name="Main" component={TabRouter} />
        <Stack.Screen name="Chat Room" component={ChatRoom} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default ScreenRouter;
