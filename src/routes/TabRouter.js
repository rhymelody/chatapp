import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Feather';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import auth from '@react-native-firebase/auth';

import {ChatDashboard, UserProfile} from '../pages/TabRouter';

const TabRouter = () => {
  const Tab = createBottomTabNavigator();
  const insets = useSafeAreaInsets();

  return (
    <Tab.Navigator
      initialRouteName="Chat"
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: EStyleSheet.value("$primary['500']"),
        tabBarInactiveTintColor: EStyleSheet.value("$secondary['500']"),
        tabBarStyle: {
          height: insets.bottom + 58,
          paddingTop: 8,
          paddingBottom: insets.bottom || 8,
        },
      }}>
      <Tab.Screen
        name="Chat"
        component={ChatDashboard}
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="message-circle" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="User"
        component={UserProfile}
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="user" size={size} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default TabRouter;
