import React, {useEffect} from 'react';
import {LogBox} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {QueryClient, QueryClientProvider} from 'react-query';
import FlashMessage from 'react-native-flash-message';
import notifee from '@notifee/react-native';
import crashlytics from '@react-native-firebase/crashlytics';
import messaging from '@react-native-firebase/messaging';
import EStyleSheet from './styles';

import {ScreenRouter} from './routes';

const App = () => {
  LogBox.ignoreAllLogs();

  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        retry: false,
      },
    },
  });

  const notificationSetUp = async () => {
    await messaging().requestPermission();
    // console.log(await messaging().getToken());
    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
    });

    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
        }
      });

    messaging().onMessage(async remoteMessage => {
      pushNotification(remoteMessage);
    });
  };

  const pushNotification = async remoteMessage => {
    const channelId = await notifee.createChannel({
      id: 'chatapp',
      name: 'Chat App',
    });

    await notifee.displayNotification({
      title: remoteMessage.notification.title,
      body: remoteMessage.notification.body,
      android: {
        channelId,
      },
    });
  };

  useEffect(() => {
    crashlytics().log('App run.');
    notificationSetUp();
    EStyleSheet.build();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <QueryClientProvider client={queryClient}>
      <SafeAreaProvider>
        <ScreenRouter />
        <FlashMessage />
      </SafeAreaProvider>
    </QueryClientProvider>
  );
};

export default App;
